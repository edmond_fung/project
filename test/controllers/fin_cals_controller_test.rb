require 'test_helper'

class FinCalsControllerTest < ActionController::TestCase
  setup do
    @fin_cal = fin_cals(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:fin_cals)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create fin_cal" do
    assert_difference('FinCal.count') do
      post :create, fin_cal: { interest: @fin_cal.interest, loan_term_id: @fin_cal.loan_term_id, principle: @fin_cal.principle }
    end

    assert_redirected_to fin_cal_path(assigns(:fin_cal))
  end

  test "should show fin_cal" do
    get :show, id: @fin_cal
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @fin_cal
    assert_response :success
  end

  test "should update fin_cal" do
    patch :update, id: @fin_cal, fin_cal: { interest: @fin_cal.interest, loan_term_id: @fin_cal.loan_term_id, principle: @fin_cal.principle }
    assert_redirected_to fin_cal_path(assigns(:fin_cal))
  end

  test "should destroy fin_cal" do
    assert_difference('FinCal.count', -1) do
      delete :destroy, id: @fin_cal
    end

    assert_redirected_to fin_cals_path
  end
end
