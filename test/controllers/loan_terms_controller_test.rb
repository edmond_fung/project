require 'test_helper'

class LoanTermsControllerTest < ActionController::TestCase
  setup do
    @loan_term = loan_terms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:loan_terms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create loan_term" do
    assert_difference('LoanTerm.count') do
      post :create, loan_term: { months: @loan_term.months, years: @loan_term.years }
    end

    assert_redirected_to loan_term_path(assigns(:loan_term))
  end

  test "should show loan_term" do
    get :show, id: @loan_term
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @loan_term
    assert_response :success
  end

  test "should update loan_term" do
    patch :update, id: @loan_term, loan_term: { months: @loan_term.months, years: @loan_term.years }
    assert_redirected_to loan_term_path(assigns(:loan_term))
  end

  test "should destroy loan_term" do
    assert_difference('LoanTerm.count', -1) do
      delete :destroy, id: @loan_term
    end

    assert_redirected_to loan_terms_path
  end
end
