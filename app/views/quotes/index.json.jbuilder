json.array!(@quotes) do |quote|
  json.extract! quote, :id, :status_id, :staff_id, :customer_id, :inventory_id
  json.url quote_url(quote, format: :json)
end
