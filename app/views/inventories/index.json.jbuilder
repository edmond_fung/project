json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :status_id, :vin, :year, :make, :model, :color, :price
  json.url inventory_url(inventory, format: :json)
end
