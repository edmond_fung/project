json.array!(@fin_cals) do |fin_cal|
  json.extract! fin_cal, :id, :principle, :interest, :loan_term_id
  json.url fin_cal_url(fin_cal, format: :json)
end
