json.array!(@loan_terms) do |loan_term|
  json.extract! loan_term, :id, :years, :months
  json.url loan_term_url(loan_term, format: :json)
end
