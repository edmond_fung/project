class LoanTermsController < ApplicationController
  before_action :set_loan_term, only: [:show, :edit, :update, :destroy]

  # GET /loan_terms
  # GET /loan_terms.json
  def index
    @loan_terms = LoanTerm.all
  end

  # GET /loan_terms/1
  # GET /loan_terms/1.json
  def show
  end

  # GET /loan_terms/new
  def new
    @loan_term = LoanTerm.new
  end

  # GET /loan_terms/1/edit
  def edit
  end

  # POST /loan_terms
  # POST /loan_terms.json
  def create
    @loan_term = LoanTerm.new(loan_term_params)

    respond_to do |format|
      if @loan_term.save
        format.html { redirect_to @loan_term, notice: 'Loan term was successfully created.' }
        format.json { render :show, status: :created, location: @loan_term }
      else
        format.html { render :new }
        format.json { render json: @loan_term.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /loan_terms/1
  # PATCH/PUT /loan_terms/1.json
  def update
    respond_to do |format|
      if @loan_term.update(loan_term_params)
        format.html { redirect_to @loan_term, notice: 'Loan term was successfully updated.' }
        format.json { render :show, status: :ok, location: @loan_term }
      else
        format.html { render :edit }
        format.json { render json: @loan_term.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /loan_terms/1
  # DELETE /loan_terms/1.json
  def destroy
    @loan_term.destroy
    respond_to do |format|
      format.html { redirect_to loan_terms_url, notice: 'Loan term was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_loan_term
      @loan_term = LoanTerm.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def loan_term_params
      params.require(:loan_term).permit(:years, :months)
    end
end
