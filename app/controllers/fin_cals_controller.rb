class FinCalsController < ApplicationController
  before_action :set_fin_cal, only: [:show, :edit, :update, :destroy]

  # GET /fin_cals
  # GET /fin_cals.json
  def index
    @fin_cals = FinCal.all
  end

  # GET /fin_cals/1
  # GET /fin_cals/1.json
  def show
    #@loan_array = @fin_cal.cal_loan_table
  end

  # GET /fin_cals/new
  def new
    @fin_cal = FinCal.new
  end

  # GET /fin_cals/1/edit
  def edit
  end

  # POST /fin_cals
  # POST /fin_cals.json
  def create
    @fin_cal = FinCal.new(fin_cal_params)

    respond_to do |format|
      if @fin_cal.save
        format.html { redirect_to @fin_cal, notice: 'Fin cal was successfully created.' }
        format.json { render :show, status: :created, location: @fin_cal }
      else
        format.html { render :new }
        format.json { render json: @fin_cal.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /fin_cals/1
  # PATCH/PUT /fin_cals/1.json
  def update
    respond_to do |format|
      if @fin_cal.update(fin_cal_params)
        format.html { redirect_to @fin_cal, notice: 'Fin cal was successfully updated.' }
        format.json { render :show, status: :ok, location: @fin_cal }
      else
        format.html { render :edit }
        format.json { render json: @fin_cal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /fin_cals/1
  # DELETE /fin_cals/1.json
  def destroy
    @fin_cal.destroy
    respond_to do |format|
      format.html { redirect_to fin_cals_url, notice: 'Fin cal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def loan
    principle = params[:principle]
    interest = params[:interest]
    loan_term = params[:loan_term]
    @loan_array = FinCal.cal_loan_table(principle.to_i, interest.to_f, loan_term.to_i)

  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_fin_cal
      @fin_cal = FinCal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def fin_cal_params
      params.require(:fin_cal).permit(:principle, :interest, :loan_term_id)
    end
end
