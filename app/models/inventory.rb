class Inventory < ActiveRecord::Base
  has_many :quotes
  has_one :status

    def self.search(search)
      if search
        where('color LIKE ? OR make LIKE? OR model LIKE? OR vin LIKE?', "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%")
      end
    end


  end

