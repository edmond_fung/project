class Customer < ActiveRecord::Base
  has_many :quotes

  def self.search(search)
    if search
      where('name LIKE ?', "%#{search}%")
    end
  end
end

