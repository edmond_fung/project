# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Customer.delete_all
Manager.delete_all
Staff.delete_all
Inventory.delete_all
Status.delete_all
Quote.delete_all
LoanTerm.delete_all
User.delete_all

Customer.create(name: "Mari Barton", phone: "(761) 462-5573", email: "sit@velitdui.co.uk")
Customer.create(name: "Jackson Yates", phone: "(409) 346-1655", email: "Integer@eleifend.ca")
Customer.create(name: "Eugenia Mcguire", phone: "(617) 723-4352", email: "Etiam.gravida.molestie@semper.edu")
Customer.create(name: "Calvin Leon", phone: "(628) 813-1684", email: "aliquet.nec.imperdiet@dui.net")
Customer.create(name: "Audra Burns", phone: "(739) 751-7516", email: "a@ac.com")

Manager.create(name: "Silas Brady", department: "Sales")
Manager.create(name: "Abigail Camacho", department: "Sales")
Manager.create(name: "Olega Silva", department: "Financial")
Manager.create(name: "Nita Hays", department: "Financial")
Manager.create(name: "Josephine Ray", department: "Inventory")

Staff.create(name: "Colleen Martin", department: "Sales")
Staff.create(name: "Melvin Goodwin", department: "Sales")
Staff.create(name: "Madison Hartman", department: "Sales")
Staff.create(name: "Quincy Francis", department: "Sales")
Staff.create(name: "Gleena Simpson", department: "Sales")

Inventory.create(status_id: "1", vin: "A2D1FDSF23", year: "2012", make: "Honda", model: "Civic", price: "15000")
Inventory.create(status_id: "1", vin: "564AFDS321", year: "2013", make: "Honda", model: "Accord", price: "25000")
Inventory.create(status_id: "1", vin: "5WQE6RQERW", year: "2015", make: "Ford", model: "Mustang", price: "30000")
Inventory.create(status_id: "1", vin: "B231EAFD563", year: "2011", make: "Ford", model: "GT", price: "50000")
Inventory.create(status_id: "1", vin: "89QEW756RW", year: "2011", make: "BMW", model: "335i", price: "45000")

Status.create(description: "Not Sold")
Status.create(description: "Sold")

Quote.create(status_id: "1", staff_id: "1", customer_id: "1", inventory_id: "1")

LoanTerm.create(years: "3", months: "36")
LoanTerm.create(years: "4", months: "48")
LoanTerm.create(years: "5", months: "60")

User.create(name: "admin", password: "admin", password_confirmation: "admin")


