class CreateFinCals < ActiveRecord::Migration
  def change
    create_table :fin_cals do |t|
      t.integer :principle
      t.float :interest
      t.integer :loan_term_id

      t.timestamps null: false
    end
  end
end
