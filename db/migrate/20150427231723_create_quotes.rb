class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :status_id
      t.integer :staff_id
      t.integer :customer_id
      t.integer :inventory_id

      t.timestamps null: false
    end
  end
end
