class CreateLoanTerms < ActiveRecord::Migration
  def change
    create_table :loan_terms do |t|
      t.integer :years
      t.integer :months

      t.timestamps null: false
    end
  end
end
