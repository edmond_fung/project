class CreateInventories < ActiveRecord::Migration
  def change
    create_table :inventories do |t|
      t.integer :status_id
      t.string :vin
      t.string :year
      t.string :make
      t.string :model
      t.string :color
      t.string :price

      t.timestamps null: false
    end
  end
end
